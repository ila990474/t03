package challenge4;

public class Main {
   public static void main(String[] args) {
      Hewan hewan = new Anjing();
      ((Anjing)hewan).suara(); // agar bisa memanggil metode suara() dari kelas Anjing
   }
}

//Type casting variabel hewan menjadi variabel Anjing